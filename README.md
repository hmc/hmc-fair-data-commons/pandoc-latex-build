# README: pandoc-latex-build


## Intro

`pandoc-latex-build` is a tool to build a pdf document from markdown source(s) 
by using pandoc and LaTeX.

You can use it as a command line tool or you can integrate it in a
CI process (cf. 
[pandoc-latex-build-demonstration](https://codebase.helmholtz.cloud/hmc/hmc-fair-data-commons/pandoc-latex-build-demonstration)
and
[pandoc-latex-build-minmal-demonstration](https://codebase.helmholtz.cloud/hmc/hmc-fair-data-commons/pandoc-latex-build-minmal-demonstration)).

`pandoc-latex-build` has a few commands described quickly in the next
subsections.

The command line program(s) provide help output by using the common flag '-h'.

This project was formerly hosted on: https://gitlab.hzdr.de/hmc/hmc-fair-data-commons/pandoc-latex-build/

### pandoc-latex-build new

This command creates a skeleton in a given project directory, which provides
the draft for your work.


### pandoc-latex-build pdf

This creates a pdf document.


### pandoc-latex-build html

This procudes a simple html page out of you markdown source(s).


## Install

See [INSTALL](INSTALL.md).


## Copyright + License

Author: Daniel Mohr, Anton Pirogov

Date: 2022-11-14

License: GNU GENERAL PUBLIC LICENSE, Version 2, June 1991

Copyright (C) 2021, 2022 Daniel Mohr, Anton Pirogov and Deutsches Zentrum fuer Luft- und Raumfahrt e. V., D-51170 Koeln

See [LICENSE](LICENSE.md).
