# LICENSE: pandoc-latex-build

Author: Daniel Mohr, Anton Pirogov.

Date: 2021-05-26.

License: GNU GENERAL PUBLIC LICENSE, Version 2, June 1991.

Copyright (C) 2021 Daniel Mohr, Anton Pirogov, Deutsches Zentrum fuer Luft- und Raumfahrt e. V. and Forschungszentrum Jülich GmbH

 This program is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 2 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy (see [gpl](gpl-2.0.txt)) of the
 GNU General Public License along with this program.
 If not, see <http://www.gnu.org/licenses/>.

## Contact Informations

 * Daniel Mohr, daniel.mohr@dlr.de

 * Anton Pirogov, a.pirogov@fz-juelich.de

## Territory

This license applies to all components/files of the software.
