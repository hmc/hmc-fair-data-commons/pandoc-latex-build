# INSTALL: pandoc-latex-build

Author: Daniel Mohr
Date: 2021-10-18

You can use pip to install `pandoc-latex-build` (preferred way) or
you can do it directly with python (cf. distutils like).


## pip


### global-install

You can use pip to install::

  pip3 install .


### home-install

You can use pip to install::

  pip3 install .

For older versions of pip you need to choose a home install explicitly::

  pip3 install --user .


### uninstall

You can use pip to uninstall::

  pip3 uninstall pandoc-latex-build


### hints

Keep in mind to have the right paths.

For the above installation to `$HOME` the software installs in::

    ~/.local/bin
    ~/.local/lib/python

Please make sure to have these paths in `$PATH` and `$PYTHONPATH`, respectively.
For example:

    export PATH=$PATH:~/.local/bin
    export PYTHONPATH=~/.local/lib/python3.6


## distutils like

You can still install `pandoc-latex-build` without using pip.
This is done in the traditional way, which was used with distutils, too.
Instead of pip only python is used.

The following subsections should work on linux.
If in doubt check the help, e. g. on linux
(on other systems, e. g. freebsd you have to replace python3 by your
python binary, e. g. python3.8):

    env python3 setup.py --help

You can also ask the installation routine/script for the necessary
python modules, e.g.:

    env python3 setup.py --requires


### global-install

To install this software globally, the following steps are to perform:

    env python3 setup.py install

You can also store the installed files, e.g.:

    env python3 setup.py install --record installed_files.txt

Then you can uninstall with this information, e.g.:

    cat installed_files.txt | xargs rm -rf


### home-install

To install this software to your `$HOME`, the following steps are to perform:

    env python3 setup.py install --home=~

You can also store the installed files, e.g.:

    env python3 setup.py install --home=~ --record installed_files.txt

Then you can uninstall with this information, e.g.:

    cat installed_files.txt | xargs rm -rf

On some systems (e. g. OSX) the flag `--home=~` is not working and you should
use instead something like `--prefix=~` or `--user`.
On some systems (e. g. opensuse) you have to set the PYTHONPATH accordingly
before installation.


### hints

Keep in mind to have the right paths.

For the above installation to `$HOME`, the software installs in:

    ~/bin
    ~/lib/python

Please make sure to have these paths in `$PATH` and `$PYTHONPATH`, respectively.
For example:

    export PATH=$PATH:~/bin
    export PYTHONPATH=~/lib/python


## hints

If you have installed [argcomplete](https://kislyuk.github.io/argcomplete/)
it is used by `pandoc-latex-build` and you can use it, 
e. g. to get bash completion:

    eval "$(register-python-argcomplete3 pandoc-latex-build)"

Similar for other shells, see
[argcomplete](https://kislyuk.github.io/argcomplete/).


## unittests

You can run all available unittests:

    env python3 setup.py run_unittest


## pytest

Instead of the standard module unittest you can also use pytest to run
available unittests:

    env python3 setup.py run_pytest

This command has a few interesting parameters, e. g.:

    env python3 setup.py run_pytest --help
    env python3 setup.py run_pytest --coverage --parallel
