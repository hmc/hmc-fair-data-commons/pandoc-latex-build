"""
:Author: Daniel Mohr
:Email: daniel.mohr@dlr.de
:Date: 2021-05-31
:License: GNU GENERAL PUBLIC LICENSE, Version 2, June 1991.

.. moduleauthor:: Daniel Mohr

"""

from .main import scripts
