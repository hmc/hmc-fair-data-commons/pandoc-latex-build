"""
:Author: Daniel Mohr
:Email: daniel.mohr@dlr.de
:Date: 2021-05-31
:License: GNU GENERAL PUBLIC LICENSE, Version 3, 29 June 2007.

tests the script 'pandoc-latex-build new'

You can run this file directly::

  env python3 pandoc-latex-build_new.py

  pytest-3 pandoc-latex-build_new.py

Or you can run only one test, e. g.::

  env python3 pandoc-latex-build_new.py ScriptNew.test_help

  pytest-3 -k test_help pandoc-latex-build_new.py
"""

import os.path
import subprocess
import tempfile
import unittest


class ScriptNew(unittest.TestCase):
    """
    :Author: Daniel Mohr
    :Date: 2021-05-31
    """

    def test_help(self):
        """
        :Author: Daniel Mohr
        :Date: 2021-05-31
        """
        with tempfile.TemporaryDirectory() as tmpdir:
            cp = subprocess.run(
                'pandoc-latex-build new -h',
                stdout=subprocess.PIPE, stderr=subprocess.PIPE,
                shell=True, cwd=tmpdir,
                timeout=3, check=True)
            self.assertTrue(cp.stdout.startswith(
                b'usage: pandoc-latex-build new '))

    def test_1(self):
        """
        :Author: Daniel Mohr
        :Date: 2021-05-31
        """
        with tempfile.TemporaryDirectory() as tmpdir:
            cp = subprocess.run(
                'pandoc-latex-build new foo',
                stdout=subprocess.PIPE, stderr=subprocess.PIPE,
                shell=True, cwd=tmpdir,
                timeout=3, check=True)
            for dn in ['foo',
                       os.path.join('foo', 'latex-template'),
                       os.path.join('foo', 'source')]:
                self.assertTrue(os.path.isdir(os.path.join(tmpdir, dn)))


if __name__ == '__main__':
    unittest.main(verbosity=2)
