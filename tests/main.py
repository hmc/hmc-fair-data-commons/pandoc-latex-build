"""
:Author: Daniel Mohr
:Email: daniel.mohr@dlr.de
:Date: 2021-05-31
:License: GNU GENERAL PUBLIC LICENSE, Version 2, June 1991.

aggregation of tests

run with::

  env python3 setup.py run_unittest

or::

  env python3 setup.py run_pytest

Or you can run this script directly (only the tests definded in this file)::

  env python3 main.py

  pytest-3 main.py

Or you can run only one test, e. g.::

  env python3 main.py TestModuleImport.test_module_import

  pytest-3 -k test_module_import main.py
"""


import unittest


class TestModuleImport(unittest.TestCase):
    """
    :Author: Daniel Mohr
    :Date: 2021-05-31
    """

    def test_module_import(self):
        """
        tests the import of the module and the submodules

        :Author: Daniel Mohr
        :Date: 2021-05-31
        """
        import pandoc_latex_build


class TestScriptExecutable(unittest.TestCase):
    """
    :Author: Daniel Mohr
    :Date: 2021-05-31
    """

    def test_script_executable(self):
        """
        test if script is executable

        env python3 main.py TestScriptExecutable.test_script_executable

        :Author: Daniel Mohr
        :Date: 2021-05-31
        """
        import subprocess
        for cmd in ['pandoc-latex-build', 'pandoc-latex-build new -h',
                    'pandoc-latex-build pdf -h', 'pandoc-latex-build html -h']:
            out = subprocess.check_output(
                cmd,
                shell=True)
            # check at least minimal help output
            self.assertTrue(len(out) >= 470)
            # check begin of help output
            self.assertTrue(out.startswith(
                b'usage: pandoc-latex-build '))


def scripts(suite):
    """
    :Author: Daniel Mohr
    :Date: 2021-05-31

    add tests for the script
    """
    print('add tests for the script(s)')
    loader = unittest.defaultTestLoader
    suite.addTest(loader.loadTestsFromTestCase(TestModuleImport))
    suite.addTest(loader.loadTestsFromTestCase(TestScriptExecutable))
    # pandoc-latex-build pdf
    suite.addTest(loader.loadTestsFromName(
        'tests.pandoc-latex-build_new'))
    # pandoc-latex-build pdf
    suite.addTest(loader.loadTestsFromName(
        'tests.pandoc-latex-build_pdf'))
    # pandoc-latex-build html
    suite.addTest(loader.loadTestsFromName(
        'tests.pandoc-latex-build_html'))


if __name__ == '__main__':
    unittest.main(verbosity=2)
