"""
:Author: Daniel Mohr
:Email: daniel.mohr@dlr.de
:Date: 2021-06-01
:License: GNU GENERAL PUBLIC LICENSE, Version 3, 29 June 2007.

tests the script 'pandoc-latex-build pdf'

You can run this file directly::

  env python3 pandoc-latex-build_pdf.py

  pytest-3 pandoc-latex-build_pdf.py

Or you can run only one test, e. g.::

  env python3 pandoc-latex-build_pdf.py ScriptPdf.test_help

  pytest-3 -k test_help pandoc-latex-build_pdf.py
"""

import os.path
import subprocess
import tempfile
import unittest


class ScriptPdf(unittest.TestCase):
    """
    :Author: Daniel Mohr
    :Date: 2021-06-01
    """

    def test_help(self):
        """
        :Author: Daniel Mohr
        :Date: 2021-05-31
        """
        with tempfile.TemporaryDirectory() as tmpdir:
            cp = subprocess.run(
                'pandoc-latex-build pdf -h',
                stdout=subprocess.PIPE, stderr=subprocess.PIPE,
                shell=True, cwd=tmpdir,
                timeout=3, check=True)
            self.assertTrue(cp.stdout.startswith(
                b'usage: pandoc-latex-build pdf '))

    def test_1(self):
        """ 
        :Author: Daniel Mohr
        :Date: 2021-06-01
        """
        with tempfile.TemporaryDirectory() as tmpdir:
            cp = subprocess.run(
                'pandoc-latex-build new foo',
                stdout=subprocess.PIPE, stderr=subprocess.PIPE,
                shell=True, cwd=tmpdir,
                timeout=3, check=True)
            cp = subprocess.run(
                'pandoc-latex-build pdf',
                stdout=subprocess.PIPE, stderr=subprocess.PIPE,
                shell=True, cwd=os.path.join(tmpdir, 'foo'),
                timeout=28, check=True)
            self.assertTrue(os.path.isfile(
                os.path.join(tmpdir, 'foo', 'document.pdf')))


if __name__ == '__main__':
    unittest.main(verbosity=2)
