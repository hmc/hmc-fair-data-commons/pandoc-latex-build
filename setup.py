"""
:Author: Daniel Mohr, Anton Pirogov
:Email: daniel.mohr@dlr.de, a.pirogov@fz-juelich.de
:Date: 2021-10-19
:License: GNU GENERAL PUBLIC LICENSE, Version 2, June 1991.
"""

from setuptools import setup, Command  # type: ignore


class TestWithPytest(Command):
    """
    :Author: Daniel Mohr
    :Email: daniel.mohr@dlr.de
    :Date: 2021-05-31

    running automatic tests with pytest
    """
    description = "running automatic tests with pytest"
    user_options = [
        ('coverage', None, 'use pytest-cov to generate a coverage report'),
        ('pytestverbose', None, 'increase verbosity of pytest'),
        ('parallel', None, 'run tests in parallel')]

    def initialize_options(self):
        """
        :Author: Daniel Mohr
        :Date: 2021-05-31
        """
        self.coverage = False
        self.pytestverbose = False
        self.parallel = False

    def finalize_options(self):
        """
        :Author: Daniel Mohr
        :Date: 2021-05-31
        """
        pass

    def run(self):
        """
        :Author: Daniel Mohr
        :Date: 2021-07-12
        """
        # env python3 setup.py run_pytest
        import sys
        import os.path
        # https://docs.pytest.org/en/stable/contents.html
        # https://pytest-cov.readthedocs.io/en/latest/
        import pytest
        pyargs = []
        if self.parallel:
            try:
                # if available, using parallel test run
                import xdist
                import sys
                if os.name == 'posix':
                    # since we are only running seconds,
                    # we use the load of the last minute:
                    nthreads = int(os.cpu_count() - os.getloadavg()[0])
                    # since we have only a few tests, limit overhead:
                    nthreads = min(4, nthreads)
                    nthreads = max(1, nthreads)  # at least one thread
                else:
                    nthreads = max(1, int(0.5 * os.cpu_count()))
                pyargs += ['-n %i' % nthreads]
            except:
                pass
        if self.coverage:
            # env python3 setup.py run_pytest --coverage
            coverage_dir = 'coverage_report/'
            # first we need to clean the target directory
            if os.path.isdir(coverage_dir):
                files = os.listdir(coverage_dir)
                for f in files:
                    os.remove(os.path.join(coverage_dir, f))
            pyargs += ['--cov=pandoc_latex_build', '--no-cov-on-fail',
                       '--cov-report=html:' + coverage_dir,
                       '--cov-report=term:skip-covered',
                       '--junitxml=report.xml',
                       ]
        if self.pytestverbose:
            pyargs += ['--verbose']
        pyargs += ['tests/main.py']
        pyargs += ['tests/pandoc-latex-build_new.py']
        pyargs += ['tests/pandoc-latex-build_pdf.py']
        pyargs += ['tests/pandoc-latex-build_html.py']
        pyplugins = []
        print('call: pytest', ' '.join(pyargs))
        status = pytest.main(pyargs, pyplugins)
        exit(status)


class TestWithUnittest(Command):
    """
    :Author: Daniel Mohr
    :Email: daniel.mohr@dlr.de
    :Date: 2021-05-31

    running automatic tests with unittest
    """
    description = "running automatic tests with unittest"
    user_options = []

    def initialize_options(self):
        """
        :Author: Daniel Mohr
        :Date: 2021-05-31
        """
        pass

    def finalize_options(self):
        """
        :Author: Daniel Mohr
        :Date: 2021-05-31
        """
        pass

    def run(self):
        """
        :Author: Daniel Mohr
        :Date: 2021-05-31
        """
        # env python3 setup.py run_unittest
        import sys
        import os.path
        sys.path.append(os.path.abspath('.'))
        import unittest
        suite = unittest.TestSuite()
        import tests
        setup_self = self

        class test_required_module_import(unittest.TestCase):
            def test_required_module_import(self):
                import importlib
                for module in setup_self.distribution.metadata.get_requires():
                    importlib.import_module(module)
        loader = unittest.defaultTestLoader
        suite.addTest(loader.loadTestsFromTestCase(
            test_required_module_import))
        tests.scripts(suite)
        unittest.TextTestRunner(verbosity=2).run(suite)


class CheckModules(Command):
    """
    :Author: Daniel Mohr
    :Date: 2017-01-08

    checking for modules need to run the software
    """
    description = "checking for modules need to run the software"
    user_options = []

    def initialize_options(self):
        pass

    def finalize_options(self):
        pass

    def run(self):
        import importlib
        summary = ""
        i = 0
        print("checking for modules need to run the software (scripts and")
        print("modules/packages) of this package:\n")
        print("checking for the modules mentioned in the 'setup.py':")
        for module in self.distribution.metadata.get_requires():
            if self.verbose:
                print("try to load %s" % module)
            try:
                importlib.import_module(module)
                if self.verbose:
                    print("  loaded.")
            except ImportError:
                i += 1
                summary += "module '%s' is not available\n" % module
                print("module '%s' is not available <---WARNING---" % module)
        print(
            "\nSummary\n%d modules are not available (not unique)\n%s\n" % (
                i, summary))


setup(
    name='pandoc-latex-build',
    version='2021.10.19',
    description='Tool to build a pdf document from markdown source(s) '
    'by using pandoc and LaTeX.',
    long_description='',
    keywords='',
    author='Daniel Mohr, Anton Pirogov',
    author_email='daniel.mohr@dlr.de, a.pirogov@fz-juelich.de',
    maintainer='',
    maintainer_email='',
    url='',
    download_url='',

    license='GNU GENERAL PUBLIC LICENSE, Version 2, June 1991',
    classifiers=[
        'Development Status :: 3 - Alpha',
        'Environment :: Console',
        'License :: OSI Approved :: GNU General Public License (GPL)',
        'Natural Language :: English',
        'Operating System :: POSIX',
        'Operating System :: POSIX :: BSD :: FreeBSD',
        'Operating System :: POSIX :: BSD :: OpenBSD',
        'Operating System :: POSIX :: Linux',
        'Operating System :: Unix',
        'Operating System :: MacOS',
        'Programming Language :: Python',
        'Programming Language :: Python :: 3'],

    package_dir={'': 'src'},
    packages=['pandoc_latex_build'],
    entry_points={
        'console_scripts': ['pandoc-latex-build=pandoc_latex_build.cli:main'],
    },
    package_data={'pandoc_latex_build': [
        'data/*.*', 'data/*/*.*', 'data/*/*/*.*']},
    requires=['appdirs',
              'argparse',
              'os',
              'platform',
              're',
              'setuptools',
              'shutil',
              'subprocess',
              'sys',
              'typing',
              'urllib.parse'],

    cmdclass={
        'run_unittest': TestWithUnittest,
        'run_pytest': TestWithPytest,
        'check_modules': CheckModules},
)
