"""
pandoc-latex-build
==================

.. contents::

Description
===========

`pandoc-latex-build` is a tool to build a pdf document from markdown source(s)
by using pandoc and latex.


Copyright + License
===================
Author: Daniel Mohr, Anton Pirogov

Date: 2021-05-26

License: GNU GENERAL PUBLIC LICENSE, Version 2, June 1991.

Copyright (C) 2021 Daniel Mohr, Anton Pirogov and Deutsches Zentrum fuer Luft- und Raumfahrt e. V., D-51170 Koeln

 This program is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 2 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License along
 with this program; if not, write to the Free Software Foundation, Inc.,
 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.

 see http://www.gnu.org/licenses/
"""

import pkg_resources

__version__ = pkg_resources.get_distribution("pandoc_latex_build").version
