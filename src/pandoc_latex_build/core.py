"""Core functionality of PLB."""

from typing import Optional

from pathlib import Path
import os
import platform
import re
import shutil
import subprocess
import sys
import urllib.parse

import appdirs  # type: ignore

import pandoc_latex_build

# to avoid typos and make it easier to change, use these:
PLB_YML: str = ".pandoc.yml"
PLB_TEX: str = ".pandoc.tex"
PLB_DEFAULT_TEMPLATE: str = "latex-template"


def get_source_files(sourcedir):
    """
    Returns an array with absolute paths of markdown files in source directory,
    sorted alphabetically.
    """
    return [
        os.path.abspath(os.path.join(sourcedir, fn))
        for fn in sorted(os.listdir(sourcedir))
        if fn.lower().endswith(".md") or fn.lower().endswith(".yml")
    ]


def get_bibfile_name(sourcedir):
    """
    Returns the absolute path of the expected BibTeX file location, if it exists.
    """
    bibfile = (Path(sourcedir) / "literature.bib").absolute()
    if bibfile.is_file():
        return str(bibfile)
    return None


def get_first_filename_with_ext(templatedir, ext):
    """
    Given a path, returns the absolute path of the alphabetically first .tex file.
    Raises RuntimeError if the path does not exist or has no .tex files.
    """
    try:
        files = os.listdir(templatedir)
        first_tex = sorted(filter(lambda fn: fn.lower().endswith(ext), files))[0]
    except FileNotFoundError:
        raise RuntimeError(f"Provided template directory {templatedir} does not exist!")
    except IndexError:
        raise RuntimeError(f"No *{ext} file found in {templatedir} !")
    return os.path.abspath(os.path.join(templatedir, first_tex))


def check_template(templatedir):
    """
    Check that at this path is something that looks like a template (certain files exist).
    Otherwise raise exception.
    """
    try:
        get_first_filename_with_ext(templatedir, PLB_YML)
        get_first_filename_with_ext(templatedir, PLB_TEX)
    except RuntimeError as err:
        raise RuntimeError("This does not look like a template:\n\t" + str(err))


def symlink_everything(srcdir, dstdir):
    """
    Symlink all files in the srcdir into dstdir (both dirs must exist).
    """
    for fname in os.listdir(srcdir):
        file_path = os.path.join(srcdir, fname)
        trg_is_dir = (platform.system() == "Windows") and (os.path.isdir(file_path))
        trg = os.path.join(dstdir, fname)
        if not os.path.exists(trg):
            os.symlink(os.path.abspath(file_path), trg, target_is_directory=trg_is_dir)


def is_template_git_repo(pathstr: str) -> bool:
    """
    We assume that pathstr is an URL with a scheme known by git or
    a scp like syntax, which git also accepts.
    """
    git_url_schemes = ("ssh", "git", "http", "https", "ftp", "ftps", "git+ssh")
    scheme = urllib.parse.urlsplit(pathstr).scheme
    if scheme in git_url_schemes:
        # URL
        return True
    elif re.findall("[^@]+:.+", pathstr):
        # scp like syntax
        return True
    else:
        return False


def is_template_name(pathstr: str) -> bool:
    """We assume that it is a proper path and not just a name if it contains /."""
    return pathstr.find("/") < 0


def resolve_template_name(name: str) -> Optional[str]:
    """
    Look for a directory to use as template in following locations (platform-specific),
    in that order:
    - current directory
    - user config directory
    - system data directory
    - built-in templates
    """
    toolname = os.path.basename(sys.argv[0])
    builtin_tdir = os.path.join(pandoc_latex_build.__path__[0], "data")  # type: ignore
    user_tdir = appdirs.user_config_dir(toolname)
    sys_tdir = appdirs.site_data_dir(toolname)

    locations = [".", user_tdir, sys_tdir, builtin_tdir]
    for loc in locations:
        path = os.path.join(loc, name)
        if os.path.isdir(path):
            return path
    return None


def create_skeleton(args):
    """
    Create a skeleton with a default template. On failure, complain and terminate.
    """
    projdir: str = args.PROJNAME
    if os.path.isdir(projdir):
        print(f"ERROR: Project directory {projdir} already exists! I refuse!")
        sys.exit(1)

    templatesrc = args.template[0]

    if is_template_git_repo(templatesrc):
        os.mkdir(projdir)
        cmd = f"git clone {templatesrc} {PLB_DEFAULT_TEMPLATE}"  # rename to default!
        try:
            subprocess.run(cmd, shell=True, cwd=os.path.abspath(projdir), check=True)
        except subprocess.CalledProcessError:
            print(f"ERROR: {cmd} failed!\nIs it really a clonable git repository?")
            print("Aborting.")
            shutil.rmtree(
                projdir
            )  # <-- CAREFUL!!! BE REALLY SURE projdir IS CORRECT!!!
            sys.exit(1)

        # the only thing in the projdir is the template, so grab it
        templatecopy = os.path.join(projdir, os.listdir(projdir)[0])

    else:
        templatesrc = os.path.relpath(templatesrc)  # <- this also removes trailing /

        # if just basename, try to resolve it in usual locations
        if is_template_name(str(templatesrc)):
            resolved_dir = resolve_template_name(str(templatesrc))
            if resolved_dir is None:
                print(f"ERROR: Could not find template {templatesrc}!")
                sys.exit(1)
            templatesrc = os.path.abspath(resolved_dir)

        # if a path was given and was not resolved, we still need to check it exists
        if not os.path.isdir(templatesrc):
            print(f"ERROR: Could not find template {templatesrc}!")
            sys.exit(1)

        # templatename = str(os.path.basename(templatesrc))
        print(f"Creating project from template {templatesrc} ...")
        templatecopy = os.path.join(projdir, PLB_DEFAULT_TEMPLATE)  # rename to default!
        shutil.copytree(str(templatesrc), templatecopy)

    # now we should have a directory that is supposedly a template
    # located at the path templatecopy
    try:
        check_template(templatecopy)
    except RuntimeError as err:
        print("ERROR: " + str(err))
        print("Aborting.")
        shutil.rmtree(projdir)  # <-- CAREFUL!!! BE REALLY SURE projdir IS CORRECT!!!
        sys.exit(1)

    # if we are here, then we can "unpack" the template...

    # copy default metadata file to source dir and canonically rename it
    os.mkdir(os.path.join(projdir, "source"))
    metafile = get_first_filename_with_ext(templatecopy, PLB_YML)
    shutil.move(metafile, os.path.join(projdir, "source", "pandoc.yml"))

    # copy example content into source dir, if it exists
    example_dirname = "template.pandoc.example"
    examplepath = os.path.join(templatecopy, example_dirname)
    if os.path.isdir(examplepath):
        for fname in os.listdir(examplepath):
            path = os.path.join(examplepath, fname)
            if os.path.isfile(path):
                shutil.move(path, os.path.join(projdir, "source", fname))
            else:
                shutil.move(path, os.path.join(projdir, "source", fname))
        # remove now empty examplepath
        os.rmdir(examplepath)


def build_pdf(args):
    """Convert to .tex, then build PDF file."""

    # prepare tex file for latexmk compilation
    build_ext("tex", args, copy_from_builddir=False)

    builddir = args.build[0]  # should exist now
    tex_filename = args.target[0] + ".tex"
    pdf_filename = args.target[0] + ".pdf"  # filename for resulting output

    # build latex in build dir
    # (produces lots of intermediate files, useful for recompilation)
    cmd = [args.latexmkcmd[0], tex_filename]
    cmd += ['-pdflatex="lualatex %O %S"', "-pdf", "-dvi-", "-ps-"]
    print(" ".join(cmd))
    subprocess.run(" ".join(cmd), shell=True, cwd=builddir, check=True)

    # retrieve resulting output pdf
    pdffile = os.path.abspath(pdf_filename)
    shutil.copy2(os.path.join(builddir, pdf_filename), pdffile)
    print(f"Location of your document: {pdffile}")


def postproc_html(args):
    """Copy images from source dir into a data directory and fix up HTML."""
    builddir = args.build[0]
    html_file = args.target[0] + ".html"

    # copy images into a data dir
    datadir = args.target[0] + "_data"
    if not os.path.isdir(datadir):
        os.mkdir(datadir)

    # grab file list for copy
    cmd = f"grep -o 'img src=\"[^\"]*\"' {html_file} | sed 's/^[^\"]*\"//;s/\"[^\"]*//'"
    imgs = subprocess.run(
        cmd, shell=True, check=True, stdout=subprocess.PIPE, encoding="utf-8"
    ).stdout.strip().split()
    print("detected images:")
    print(imgs)

    for img in imgs:
        head, _ = os.path.split(img)
        if bool(head):
            # image is in subdirectory, need to create it
            os.makedirs(os.path.join(datadir, head), exist_ok=True)
        shutil.copy2(os.path.realpath(os.path.join(builddir, img)),
                     os.path.join(datadir, img))

    # rewrite image paths to point into the datadir
    cmd = f"sed -E -i 's/(img src=\")/\\1{datadir}\\//' {html_file}"
    subprocess.run(cmd, shell=True, check=True)
    print(f"Relinked images in HTML to point to {datadir}.")


def build_ext(ext: str, args, copy_from_builddir=True):
    """Build document.ext using pandoc with suitable flags."""

    # common preparations: symlink sources to build dir
    sourcedir = args.source[0]
    if not os.path.isdir(sourcedir):
        print(f"ERROR: Source directory {sourcedir} does not exist!")
        sys.exit(1)

    builddir = args.build[0]
    if not os.path.isdir(builddir):
        os.mkdir(builddir)
    symlink_everything(sourcedir, builddir)

    # If we support a template for that type...
    templatefile: Optional[str] = None
    if ext == "tex":
        templatedir = args.template[0]
        if not os.path.isdir(templatedir):
            print(f"ERROR: Template directory {templatedir} does not exist!")
            sys.exit(1)
        symlink_everything(templatedir, builddir)

        # find main tex file (will raise exception and abort on failure)
        templatefile = get_first_filename_with_ext(templatedir, PLB_TEX)

    # convert using pandoc
    result_file = args.target[0] + "." + ext

    cmd = [args.pandoccmd[0]]
    cmd += get_source_files(sourcedir)

    bibfile = get_bibfile_name(sourcedir)
    if bibfile is not None:
        cmd += [f"--bibliography={bibfile}"]

    cmd += ["-s", "-o", result_file]
    # ext-specific settings
    if ext == "html":
        cmd += ["--mathjax"]
    elif ext == "tex":
        assert templatefile is not None
        cmd += ["--listings", "--natbib"]
        cmd += ["--template=" + templatefile]

    print(" ".join(cmd))
    subprocess.run(" ".join(cmd), shell=True, cwd=builddir, check=True)

    # retrieve resulting output
    if copy_from_builddir:
        shutil.copy2(os.path.join(builddir, result_file), result_file)
        print(f"Location of your document: {os.path.abspath(result_file)}")

        if ext == "html":  # fix up images
            postproc_html(args)
