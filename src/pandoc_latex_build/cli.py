#!/usr/bin/env python3
"""
:Author: Daniel Mohr, Anton Pirogov
:Email: daniel.mohr@dlr.de, a.pirogov@fz-juelich.de
:Date: 2021-06-01
:License: GNU GENERAL PUBLIC LICENSE, Version 2, June 1991

This exposes the entry point for the command line utility that is installed.
"""

try:
    import argcomplete  # type: ignore
except (ModuleNotFoundError, ImportError):
    pass
import argparse
import os
import sys

import pandoc_latex_build
import pandoc_latex_build.core as core


def main():
    """Entry point for the pandoc-latex-build tool."""
    TOOLNAME = os.path.basename(sys.argv[0])  # pylint: disable=C0103

    epilog = (
        "Examples:\n"
        f"  {TOOLNAME} new my_project\n"
        f"  {TOOLNAME} new my_project -template path/to/template\n"
        f"  {TOOLNAME} new my_project -template template-name\n"
        f"  {TOOLNAME} new my_project -template https://url.clonable.by.git\n"
        f"  {TOOLNAME} pdf\n"
        f"  {TOOLNAME} html\n\n"
        "Author: Daniel Mohr, Anton Pirogov\n"
        f"Version: {pandoc_latex_build.__version__}\n"
        "\n\n"
    )

    description = f'"{TOOLNAME}" is a tool to build a document from markdown files.'

    parser = argparse.ArgumentParser(
        description=description,
        epilog=epilog,
        formatter_class=argparse.RawDescriptionHelpFormatter,
    )

    common_parser = argparse.ArgumentParser(add_help=False)
    common_parser2 = argparse.ArgumentParser(add_help=False)
    common_parser.add_argument(
        "-target",
        nargs=1,
        type=str,
        required=False,
        default=["document"],
        dest="target",
        help="Target name of the output document (without file extension); "
        "default: document",
    )
    common_parser.add_argument(
        "-source",
        nargs=1,
        type=str,
        required=False,
        default=["source"],
        dest="source",
        help="Source directory with markdown files, relative to current directory. "
        "default: source",
    )
    common_parser.add_argument(
        "-pandoccmd",
        nargs=1,
        type=str,
        required=False,
        default=["pandoc"],
        dest="pandoccmd",
        help="pandoc command to use; default: pandoc",
    )
    common_parser.add_argument(
        "-build",
        nargs=1,
        type=str,
        required=False,
        default=["build"],
        dest="build",
        help="Describe the build directory relative to current directory; "
        "default: build",
    )

    common_parser2.add_argument(
        "-template",
        nargs=1,
        type=str,
        required=False,
        default=[core.PLB_DEFAULT_TEMPLATE],
        dest="template",
        help="Template directory relative to current directory; "
        f"There should be only one file ending with {core.PLB_TEX}, "
        "which will be used. You can also use URLs, which git understand "
        "(e. g. git+ssh://user@host.xz/path/repo.git). "
        f"default: {core.PLB_DEFAULT_TEMPLATE}",
    )

    subparsers = parser.add_subparsers(
        dest="subparser_name",
        help="There are different targets as sub-commands available.",
    )

    parser_new = subparsers.add_parser(
        "new",
        description="Create a skeleton in a subdirectory of current directory "
        "based on a default template.",
        formatter_class=argparse.RawDescriptionHelpFormatter,
        help=f"create skeleton, see: {TOOLNAME} new -h",
        parents=[common_parser2],
    )
    parser_new.add_argument("PROJNAME", help="Name of new project.")
    parser_new.set_defaults(func=core.create_skeleton)

    parser_pdf = subparsers.add_parser(
        "pdf",
        description="build pdf",
        formatter_class=argparse.RawDescriptionHelpFormatter,
        help=f"build pdf, see: {TOOLNAME} pdf -h",
        parents=[common_parser, common_parser2],
    )
    parser_pdf.add_argument(
        "-latexmkcmd",
        nargs=1,
        type=str,
        required=False,
        default=["latexmk"],
        dest="latexmkcmd",
        help="latexmk command to use; default: latexmk",
    )
    parser_pdf.set_defaults(func=core.build_pdf)

    def add_ext(ext):
        ext_parser = subparsers.add_parser(
            ext,
            description="build {ext} (experimental)",
            formatter_class=argparse.RawDescriptionHelpFormatter,
            help=f"build {ext} (experimental), see: {TOOLNAME} {ext} -h",
            parents=[common_parser, common_parser2],
        )
        ext_parser.set_defaults(func=lambda args: core.build_ext(ext, args))
    add_ext("html")
    add_ext("odt")
    add_ext("icml")

    try:
        argcomplete.autocomplete(parser)
    except NameError:
        pass

    args = parser.parse_args()
    if args.subparser_name is not None:
        args.func(args)  # call the programs
    else:
        parser.print_help()


if __name__ == "__main__":
    main()
